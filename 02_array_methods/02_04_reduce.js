// Learning Function Programming with Javascript
// Chapter 03, Video 04, Exercise 04

const _ = require('lodash')

const sumCosts = (acc, { price, quantity }) => acc + price * quantity

const shoppingList = [
  { name: "Egg",    price: 1.45, quantity: 5 },
  { name: "Bread",  price: 8.40, quantity: 1 },
  { name: "Orange", price: 5.95, quantity: 2 }
]

const totalCost = _.reduce(shoppingList, sumCosts, 0)

console.log(totalCost)