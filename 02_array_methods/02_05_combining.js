// Learning Function Programming with Javascript
// Chapter 03, Video 05, Exercise 05

const _ = require('lodash')

// Atomic functions
const filterByGenre = genre => ({ genre: sex }) => sex === genre
const mapSalary = ({ salary }) => salary
const getSum = (accum, current) => accum + current

// Combiner function to get the average salary of a group
// of employees depending the genre ('M' for male and 'F' for female)
const avgSalaryByGenre = genre => {
  return employees => {
    const sample = employees.length

    // Filter the employees by genre
    const filteredByGenre = _.filter(employees, filterByGenre(genre))
    // Map to array of salaries
    const mappedSalary = _.map(filteredByGenre, mapSalary)
    // Sum up the salaries
    const summedSalary = _.reduce(mappedSalary, getSum)
    // Get the average salary for ${genre} employees
    const avgSalary = summedSalary / sample

    return avgSalary
  }
}

// Functions to get the Average salary
const getAvgSalaryMale = avgSalaryByGenre('M')   // Male
const getAvgSalaryFemale = avgSalaryByGenre('F') // Female

const employees = [
  { name: 'Jonathan', salary: 7000, position: 'CEO', genre: 'M' },
  { name: 'Romanov',  salary: 8500, position: 'DBA', genre: 'F' },
  { name: 'Alice',    salary: 8930, position: 'PM',  genre: 'F' },
  { name: 'Stephan',  salary: 2840, position: 'CTO', genre: 'F' },
  { name: 'George',   salary: 7800, position: 'DM',  genre: 'M' },
  { name: 'Mary',     salary: 2840, position: 'DM',  genre: 'F' }
]


const avgSalaryMale = getAvgSalaryMale(employees)
const avgSalaryFemale = getAvgSalaryFemale(employees)

console.log(`Male average salary: ${avgSalaryMale}`)
console.log(`Female average salary: ${avgSalaryFemale}`)
