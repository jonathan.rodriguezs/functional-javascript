// Learning Function Programming with Javascript
// Chapter 03, Video 03, Exercise 03

const _ = require('lodash')

const lessThan7K = quantity => quantity < 7000
const earnsLessThan7K = employee => lessThan7K(employee.salary)

const employees = [
  { name: 'Jonathan', salary: 7000 },
  { name: 'Romanov',  salary: 4500 },
  { name: 'Alice',    salary: 8930 },
  { name: 'Stephan',  salary: 2840 },
]

const raiseProspects = _.filter(employees, earnsLessThan7K)

console.log(raiseProspects)