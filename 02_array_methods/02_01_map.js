// Learning Function Programming with Javascript
// Chapter 03, Video 01, Exercise 01

const _ = require('lodash')

const numbers = [1, 2, 3, 4, 5]
const squared = _.map(numbers, x => x * x)

console.log(squared)