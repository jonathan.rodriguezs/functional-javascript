// Learning Function Programming with Javascript
// Chapter 03, Video 02, Exercise 02

const _ = require('lodash')

const checkPosition = position => employee => employee.position === position
const isDBA = checkPosition('DBA')
const isDBA = employee => checkDBA(employee)

const earnsMoreThan2K = ({ salary }) => salary > 2000

const isDBAandEarnMoreThan2K = employee => isDBA(employee) && earnsMoreThan2K(employee)

const employees = [
  { name: 'Jonathan', salary: 7000, position: 'CEO' },
  { name: 'Romanov',  salary: 8500, position: 'DBA'},
  { name: 'Alice',    salary: 8930, position: 'PM' },
  { name: 'Stephan',  salary: 2840, position: 'CTO' },
]

const DBAs = _.some(employees, isDBAandEarnMoreThan2K)

console.log(DBAs)