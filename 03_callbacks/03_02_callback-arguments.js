// Learning Function Programming with Javascript
// Chapter 04, Video 02, Exercise 02

const fs = require('fs')

const logFile = (err, data) => {
  if (err) throw err
  console.log(data)
}

fs.readFile('message.txt', 'utf-8', logFile)