// Learning Function Programming with Javascript
// Chapter 04, Video 01, Exercise 01

let x = 150

console.log(`x is orgininally: ${x}`) // 1

setTimeout(() => {
  x = 3000
  console.log(`x has been changed to: ${x}`) // 3
}, 3000)

console.log(`Now the x value is: ${x}`) // 2