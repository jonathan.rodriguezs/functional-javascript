// Learning Function Programming with Javascript
// Chapter 02, Video 03, Exercise 03

function doIf(condition, func) {
  condition && func()
}

const x = 1
doIf(x === 1, () => console.log("x is equal to 1"))
doIf(x === "Bananas", () => console.log("x is equal to 'Bananas'"))
doIf(x === true, () => console.log("x is equal to true"))