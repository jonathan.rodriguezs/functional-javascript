## Concepts

- *High-Order Function*:
  A function that takes a function as an argument, returns a function, or both.