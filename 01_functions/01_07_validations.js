// Learning Function Programming with Javascript
// Chapter 02, Video 05, Exercise 06

function safeCall(func) {
  return function(number, message) {
    if(number != null && typeof number === "number") {
      if(message != null && typeof message === "string") {
        return func(number, message)
      }
    }
  }
}

function printMessageNTimes(n, message) {
  for(let i = 0; i < n; i++)
    console.log(message)
}

function getNthLetter(n, string) {
  return string.charAt(n)
}

function getSubstringOfLenght(func) {
  return function(n, string) {
    return func(string.substring(0, n))
  }
}

const printMessageNTimesSafe = safeCall(printMessageNTimes)
const getNthLetterSafe = safeCall(getNthLetter)
const getSubstringOfLenghtSafe = safeCall(getSubstringOfLenght(console.log))

printMessageNTimesSafe(3, "Banana") // Banana Banana Banana
getNthLetterSafe(2, "Javascript")   // v
getSubstringOfLenghtSafe(5, 'Hello and welcome') // Hello
