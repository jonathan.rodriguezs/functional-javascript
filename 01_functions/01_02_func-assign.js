// Learning Function Programming with Javascript
// Chapter 02, Video 02, Exercise 02

const DEBUG_MODE_ENABLED = process.argv[2] !== "false" // disable debugger by arg
const printDebug = message => { console.log("DEBUG: " + message) }
const doNothing = () => {}

let debug
if(DEBUG_MODE_ENABLED) {
  // just print to console if DEBUG is enabled
  debug = printDebug
} else {
  debug = doNothing
}

debug("I'm debugging, wish me luck")