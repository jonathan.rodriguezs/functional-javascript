// Learning Function Programming with Javascript
// Chapter 02, Video 02, Exercise 01

const print = console.log // Function expression
const breakLine = (lines = 1) => {
  for(_ in [...Array(Math.abs(lines))])
    print()
}

print("LADY MACBETH")
print("W. Shakespeare")

breakLine(2)

print("Seargeant:")
print("Doubtful it stood;")
print("As two spent swimmers, that do cling together")

breakLine(1)

print("DUNCAN")
print("O valiant cousin! worthy gentleman!")