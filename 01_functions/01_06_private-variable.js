// Learning Function Programming with Javascript
// Chapter 02, Video 04, Exercise 06

function createCounter(count = 0) {
  return {
    increment: () => { count += 1 },
    currentValue: () => count
  }
}

const myCounter = createCounter(900)

console.log(myCounter.currentValue())

myCounter.increment()
myCounter.increment()

console.log(myCounter.currentValue())

// this doesn't work anymore
myCounter.count = 999

console.log(myCounter.currentValue())
