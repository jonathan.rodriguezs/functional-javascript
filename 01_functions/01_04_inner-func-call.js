// Learning Function Programming with Javascript
// Chapter 02, Video 03, Exercise 04

function callWith2Arguments(arg1, arg2, func) {
  return func(arg1, arg2)
}

callWith2Arguments(1, 2, add)
callWith2Arguments(9, 4, substract)
callWith2Arguments(2, 4, (x, y) => x * y)


function add(x, y) {
  return x + y
}

function substract(x, y) {
  return x - y
}