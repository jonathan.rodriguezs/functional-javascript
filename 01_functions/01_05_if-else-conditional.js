// Learning Function Programming with Javascript
// Chapter 02, Video 03, Exercise 05

function ifElse(condition, func1, func2) {
  condition ? func1(): func2()
}

const savings = 10403

ifElse(
  savings > 12903,
  () => console.log("Let's go to vacation!"),
  () => console.log("Maybe the next time")
)